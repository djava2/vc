﻿using System;

namespace VerificaCartao
{
	public class VerificaCartao
	{
		public static void Main (string[] args) {
			String numCartao;
			String tipo = "Desconhecido";

			numCartao = Console.ReadLine();
			numCartao = numCartao.Replace (" ", String.Empty);

			if (numCartao.StartsWith("34") || numCartao.StartsWith("37") && numCartao.Length == 15) {
				tipo = "AMEX";
			} else if (numCartao.StartsWith("6011") && numCartao.Length == 16) {
				tipo = "Discover";
			} else if (numCartao.StartsWith("4") && (numCartao.Length == 13 || numCartao.Length == 16)) {
				tipo = "VISA";
			} else {
				for (int i = 51; i < 56; i++) {
					if (numCartao.StartsWith(i.ToString()) && numCartao.Length == 16) {
						tipo = "MasterCard";
						break;
					}
				}
			}

			Console.Write(tipo + ": " + numCartao);
			Console.WriteLine(Luhn.check(numCartao) ? " (válido)" : " (inválido)");
		}
	}
}

