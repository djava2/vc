﻿using System;

namespace VerificaCartao
{
	public class Luhn
	{
		public static Boolean check(String ccNumber)
		{
			int sum = 0;
			Boolean alternate = false;
			for (int i = ccNumber.Length - 1; i >= 0; i--)
			{
				int n = Convert.ToInt32(ccNumber.Substring(i, 1));
				if (alternate)
				{
					n *= 2;
					if (n > 9)
					{
						n = (n % 10) + 1;
					}
				}
				sum += n;
				alternate = !alternate;
			}
			return (sum % 10 == 0);
		}
	}
}

